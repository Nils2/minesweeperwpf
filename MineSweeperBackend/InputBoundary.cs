﻿using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend {
    public interface InputBoundary {
        void startGame(int gameFieldWidth, int gameFieldHeight, int numberOfMines);
        void subscribeToGameFieldElements(IObserver<State>[,] uiElements);
        void leftClick(Point targetPosition);
        void rightClick(Point targetPosition);
    }
}
