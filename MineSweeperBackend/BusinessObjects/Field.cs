﻿using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.BusinessObjects {
    class Field : IObservable<State> {
        private Point position;
        private State stateOfField;
        private IObserver<State> uiElementObserver;

        public Field(Point position, State initialState) {
            this.position = position;
            stateOfField = initialState;
        }

        public Point getPosition() {
            return position;
        }

        public IDisposable Subscribe(IObserver<State> observer) {
            uiElementObserver = observer;
            return new Unsubscribe(observer);
        }

        public void updateState(State state) {
            stateOfField = state;
            if (uiElementObserver != null) {
                uiElementObserver.OnNext(state);
            }
        }

        public State getState() {
            return stateOfField;
        }
        
        private class Unsubscribe : IDisposable {
            private IObserver<State> observer;

            public Unsubscribe(IObserver<State> observer) {
                this.observer = observer;
            }

            public void Dispose() {
                if(observer != null) {
                    observer = null;
                }
            }
        }

        // for testing purposes
        public IObserver<State> getObserver() {
            return this.uiElementObserver;
        }
    }
}
