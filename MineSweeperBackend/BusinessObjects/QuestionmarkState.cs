﻿using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.BusinessObjects {
    /// <summary>
    /// State where the field shows a questionmark meaning the user is unsure
    /// </summary>
    class QuestionmarkState : State{
        private static string name = "Questionmark";
        private static Color color = Color.Aquamarine;

        public QuestionmarkState() : base(color, name) {
        }
    }
}
