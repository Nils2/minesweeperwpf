﻿using MineSweeperBackend.BusinessObjects.Factories;
using MineSweeperBackend.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.BusinessObjects {
    class GameField {
        Field[,] fields;
        int width;
        int height;

        public GameField(int width, int height) {
            this.width = width;
            this.height = height;
            initializeGameField();
        }

        private void initializeGameField() {
            fields = new Field[width, height];
            FieldFactory fieldFactory = new FieldFactory();
            for(int x = 0; x < width; x++) {
                for(int y = 0; y < height; y++) {
                    Point position = new Point(x, y);
                    fields[x, y] = fieldFactory.createInstance(position);
                }
            }
        }

        internal Field[,] getGameField() {
            return fields;
        }

        public Field this[Point position] {
            get {
                return fields[position.X, position.Y];
            }
        }

        public int Width {
            get { return width; }
        }

        public int Height {
            get { return height; }
        }

        public bool isInGameField(int xPosition, int yPosition) {
            return !( xPosition < 0 || xPosition >= width || yPosition < 0 || yPosition >= height );
        }
    }
}
