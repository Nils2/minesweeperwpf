﻿using MineSweeperBackend.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.BusinessObjects.Factories {
    class FieldFactory {
        public Field createInstance(Point position) {
            return new Field(position, new DefaultState());
        }
    }
}
