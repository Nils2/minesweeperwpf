﻿using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.BusinessObjects {
    /// <summary>
    /// Default state of any button at the start of the game
    /// </summary>
    class DefaultState : State{
        private static Color color = Color.AliceBlue;
        private static string name = "default";

        public DefaultState() : base(color, name){ }
    }
}
