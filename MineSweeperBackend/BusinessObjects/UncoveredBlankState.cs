﻿using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MineSweeperBackend.BusinessObjects {
    /// <summary>
    /// State when the Field was uncovered and no neighboring Mines are Present
    /// </summary>
    class UncoveredBlankState : State {
        private static string name = "blank";
        private static Color color = Color.White;

        public UncoveredBlankState() : base(color, name) { }
    }
}
