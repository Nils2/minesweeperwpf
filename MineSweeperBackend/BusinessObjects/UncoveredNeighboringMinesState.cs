﻿using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MineSweeperBackend.BusinessObjects {
    /// <summary>
    /// State when the field was uncovered and no neighboring mines where found
    /// </summary>
    class UncoveredNeighboringMinesState : State {
        private static string name = "uncovered";
        private static Color color = Color.Coral;

        public UncoveredNeighboringMinesState(int numberOfNeighboringMines) : base(color, name) {
            base.Text = numberOfNeighboringMines.ToString();
        }
    }
}
