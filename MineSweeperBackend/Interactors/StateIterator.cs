﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Interactors {
    /// <summary>
    /// Runs through the diffrent possible states of a field when the user right-clicks the field
    /// </summary>
    class StateIterator {
        private List<State> states;

        public StateIterator(List<State> possibleStates) {
            states = possibleStates;
        }

        public State next(State currentState) {
            int iteratorIndex = getIteratorPosition(currentState.Name);
            // from: http://stackoverflow.com/questions/776725/list-get-next-element-or-get-the-first
            int nextIteratorIndex = getPositionOfNextState(iteratorIndex);
            return states[nextIteratorIndex];
        }

        private int getIteratorPosition(string stateName) {
            // from: http://stackoverflow.com/questions/17995706/how-to-get-the-index-of-an-item-in-a-list-in-a-single-step
            return states.FindIndex(a => a.Name == stateName);
        }

        private int getPositionOfNextState(int currentPosition) {
            if (currentPosition < 0)
                return 0;
            // % operator prevents outofRange and ++ operator is needed to get to the next stage
            return ++currentPosition % states.Count;
        }
    }
}
