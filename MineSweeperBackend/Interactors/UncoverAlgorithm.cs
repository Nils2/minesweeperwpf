﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Interactors {
    class UncoverAlgorithm {
        HashSet<Point> searchedFields;
        Queue<Point> toSearch;
        GameField gamefield;
        NeighbourFinder neighbours;
        HashSet<Point> existingMines;

        public UncoverAlgorithm(GameField gamefield, HashSet<Point> existingMines) {
            this.gamefield = gamefield;
            neighbours = new NeighbourFinder();
            this.existingMines = existingMines;
        }

        /// <summary>
        /// Uncovers all connected elements from the field the user clicked with no mines on them.
        /// sends automatically the new status to the uiElements via observer-pattern
        /// </summary>
        /// <param name="clicked">The Position of the field the user clicked</param>
        public void uncover(Point clicked) {
            searchedFields = new HashSet<Point>();
            toSearch = new Queue<Point>();
            toSearch.Enqueue(clicked);
            searchThroughAllFields();
        }

        private void searchThroughAllFields() {
            while(toSearch.Count > 0) {
                Point field = toSearch.Dequeue();
                if (!searchedFields.Contains(field)) {
                    int neighboringMines = neighbours.getNumberOfNeighboringMines(existingMines, gamefield, field);
                    if (neighboringMines == 0) {
                        addNeighboursToSearch(field);
                        gamefield[field].updateState(new UncoveredBlankState());
                    }
                    else {
                        gamefield[field].updateState(new UncoveredNeighboringMinesState(neighboringMines));
                    }
                    searchedFields.Add(field);
                }
            }
        }

        private void addNeighboursToSearch(Point field) {
            HashSet<Point> neighboringFields = neighbours.getNeighbours(gamefield, field);
            foreach (Point point in neighboringFields) {
                toSearch.Enqueue(point);
            }
        }
    }
}
