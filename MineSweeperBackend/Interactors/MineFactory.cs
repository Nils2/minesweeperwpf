﻿using MineSweeperBackend.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.Exceptions;

namespace MineSweeperBackend.Interactors {
    class MineFactory {
        private GameField field;
        private HashSet<Point> mines;
        private Random random;

        public MineFactory(GameField gameField) {
            field = gameField;
            random = new Random();
        }

        internal HashSet<Point> generate(int numberOfMines) {
            if (isOutOfBoundary(numberOfMines)) {
                throw new OutOfGameFieldBoundaryException();
            }
            mines = new HashSet<Point>();
            setMines(numberOfMines);
            return mines;
        }

        private void setMines(int numberOfMines) {     
            while(mines.Count < numberOfMines) {
                Point nextMine = generateMineAtRandomPosition();
                mines.Add(nextMine);
            }
        }

        private bool isOutOfBoundary(int numberOfMines) {
            return (numberOfMines < 0 || numberOfMines > ( field.Width * field.Height )) ;
        }

        private Point generateMineAtRandomPosition() {
            int randomX = random.Next(0, field.Width);
            int randomY = random.Next(0, field.Height);
            return new Point(randomX, randomY);
        }
    }
}
