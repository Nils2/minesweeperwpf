﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;

namespace MineSweeperBackend.Interactors {
    /// <summary>
    /// Searches all the neighbours of a single Field in the gamefield
    /// </summary>
    class NeighbourFinder {
        private HashSet<Point> neighbours;
        private GameField gameField;

        /// <summary>
        /// Searches all the neighboring fields of center and returns them in a hashset
        /// </summary>
        /// <param name="field">the gamefield</param>
        /// <param name="center">the center from witch the algorithm will search for neighbours</param>
        /// <returns>A Hashset with the points of the neighbours</returns>
        internal HashSet<Point> getNeighbours(GameField field, Point center) {
            gameField = field;
            neighbours = new HashSet<Point>();
            foreach(Point neighbour in getListOfPotentialNeighboringPoints(center)) {
                addNeighbour(neighbour);
            }
            return neighbours;
        }

        /// <summary>
        /// iterates through all possible neighbours and adds them to the hashset
        /// </summary>
        /// <param name="center"></param>
        private HashSet<Point> getListOfPotentialNeighboringPoints(Point center) {
            HashSet<Point> potentialNeighbours = new HashSet<Point>();

            potentialNeighbours.Add(new Point(center.X - 1, center.Y - 1));
            potentialNeighbours.Add(new Point(center.X, center.Y - 1));
            potentialNeighbours.Add(new Point(center.X + 1, center.Y - 1));
            potentialNeighbours.Add(new Point(center.X - 1, center.Y));
            potentialNeighbours.Add(new Point(center.X + 1, center.Y));
            potentialNeighbours.Add(new Point(center.X - 1, center.Y + 1));
            potentialNeighbours.Add(new Point(center.X, center.Y + 1));
            potentialNeighbours.Add(new Point(center.X + 1, center.Y + 1));

            return potentialNeighbours;
        }

        /// <summary>
        /// Adds a neighbour to the hashset if the neighbour is within the gamefield-range
        /// </summary>
        /// <param name="xPosition">xPosition of the potential neighbour</param>
        /// <param name="yPosition">yPosition of the potential neighbour</param>
        private void addNeighbour(Point neighbour) {
            if(gameField.isInGameField(neighbour.X, neighbour.Y)) {
                neighbours.Add(gameField[neighbour].getPosition());
            }
        }

        /// <summary>
        /// Checks, how many mines are in the neighboring area of the center
        /// </summary>
        /// <param name="mines">Hashset with all the mines on the gamefield</param>
        /// <param name="gamefield">the gamefield</param>
        /// <param name="center">center from which the algorithm will search</param>
        /// <returns>the Number of neighboring mines from the center</returns>
        public int getNumberOfNeighboringMines(HashSet<Point> mines, GameField gamefield, Point center) {
            HashSet<Point> neighbours = getNeighbours(gamefield, center);
            neighbours.IntersectWith(mines);
            return neighbours.Count;
        }
    }
}
