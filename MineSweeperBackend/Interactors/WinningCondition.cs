﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Interactors {
    class WinningCondition {
        private HashSet<Point> mines;
        private GameField gamefield;

        private HashSet<Type> needUncoveringTypes; // contains all types that a user must uncover in order to win the game

        public WinningCondition() {
            needUncoveringTypes = new HashSet<Type>();
            needUncoveringTypes.Add(new DefaultState().GetType());
            needUncoveringTypes.Add(new QuestionmarkState().GetType());
            needUncoveringTypes.Add(new FlaggedState().GetType());
        }

        /// <summary>
        /// Checks if the user needs to uncover more fields
        /// </summary>
        /// <param name="gamefield">The Gamefield</param>
        /// <param name="mines">The existing mines in the gamefield</param>
        /// <returns>True if user uncovered all neccessary Fields; False if user needs to uncover more</returns>
        public bool isGameWon(GameField gamefield, HashSet<Point> mines) {
            this.gamefield = gamefield;
            this.mines = mines;
            for(int i = 0; i < gamefield.Width; i++) {
                for(int j = 0; j < gamefield.Height; j++) {
                    Point currentPosition = new Point(i, j);
                    if (hasToBeUncovered(currentPosition)) {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Checks if the user still needs to uncover the Field
        /// </summary>
        /// <param name="position">position of the Field that is checked</param>
        /// <returns>True is user needs to uncover the Field, False if the Field is already uncovered </returns>
        private bool hasToBeUncovered(Point position) {
            State stateOfField = gamefield[position].getState();
            return ( !mines.Contains(position) && needUncoveringTypes.Contains(stateOfField.GetType()) );
        }
    }
}
