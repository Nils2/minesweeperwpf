﻿namespace MineSweeperBackend.RequestModels {
    public class Point {
        int x; // x position in coord.System
        int y; // y position in coord.System

        public Point(int xPosition, int yPosition) {
            x = xPosition;
            y = yPosition;
        }

        public int X {
            get { return x; }
        }

        public int Y {
            get { return y; }
        }

        public static bool operator ==(Point first, Point second) {
            return (first.x == second.x && first.y == second.y) ;
        }
        
        public static bool operator !=(Point first, Point second) {
            return !(first == second);
        }

        public override bool Equals(object obj) {
            return this == (Point) obj;
        }

        public override int GetHashCode() {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
                return hash;
            }
        }
    }
}
