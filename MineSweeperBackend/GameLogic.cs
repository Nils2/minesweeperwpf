﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.Interactors;
using MineSweeperBackend.Exceptions;

namespace MineSweeperBackend {
    class GameLogic : InputBoundary {
        private GameField gamefield;
        private StateIterator stateIterator;
        private HashSet<Point> mines;
        private UncoverAlgorithm uncoverAlgorithm;
        private OutputBoundary userInterface;
        private WinningCondition winningCondition;

        public GameLogic(OutputBoundary userInterface) {
            this.userInterface = userInterface;
            winningCondition = new WinningCondition();
        }

        public void leftClick(Point targetPosition) {
            Field targetField = gamefield[targetPosition];
            if (positionIsNotDefaultState(targetPosition)) {
                return; // do nothing
            }
            if (mines.Contains(targetPosition)) {
                userInterface.gameOver();
            }
            uncoverAlgorithm = new UncoverAlgorithm(gamefield, mines);
            uncoverAlgorithm.uncover(targetPosition);
            if(winningCondition.isGameWon(gamefield, mines)) {
                userInterface.gameWon();
            }
        }

        private bool positionIsNotDefaultState(Point fieldPosition) {
            Field field = gamefield[fieldPosition];
            Type typeOfField = field.getState().GetType();
            return (typeOfField != typeof(DefaultState) );
        }

        public void rightClick(Point targetPosition) {
            Field selectedField = gamefield[targetPosition];
            State nextStateOfField = stateIterator.next(selectedField.getState());
            selectedField.updateState(nextStateOfField);
        }

        public void startGame(int gameFieldWidth, int gameFieldHeight, int numberOfMines) {
            gamefield = new GameField(gameFieldWidth, gameFieldHeight);
            MineFactory mineFactory = new MineFactory(gamefield);
            mines = mineFactory.generate(numberOfMines);
            initializeStateIterator();
        }

        private void initializeStateIterator() {
            List<State> possibleStatesForRightClick = new List<State>();
            possibleStatesForRightClick.Add(new DefaultState());
            possibleStatesForRightClick.Add(new FlaggedState());
            possibleStatesForRightClick.Add(new QuestionmarkState());
            stateIterator = new StateIterator(possibleStatesForRightClick);
        }

        public void subscribeToGameFieldElements(IObserver<State>[,] uiElements) {
            for(int i = 0; i < gamefield.getGameField().GetLength(0); i++) {
                for(int j = 0; j < gamefield.getGameField().GetLength(1); j++) {
                    Point position = new Point(i, j);
                    gamefield[position].Subscribe(uiElements[i, j]);
                }
            }
        }


        // these testing methods are not the optimal solution, but since they are not in the Input-Boundary-Interface the
        // UI should not be able to use them.
        // for testing purposes
        internal GameField getGameField() {
            return gamefield;
        }

        // for testing purposes
        internal HashSet<Point> getMInes() {
            return mines;
        }

        // for testing purposes
        internal StateIterator getStateIterator() {
            return stateIterator;
        }

        // for testing purposes
        internal void setValuesForTests(GameField gamefield, HashSet<Point> mines) {
            this.gamefield = gamefield;
            this.mines = mines;
        }
    }
}
