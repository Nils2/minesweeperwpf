﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.ResponseModels {
    public class State {
        private Color color; // color that represents status --> maybe change it to an image later
        private string name;
        private string text; // Text that will be displayed in the field

        public State() {
        }

        protected State(Color color, string name) {
            this.color = color;
            this.name = name;
        }

        public Color StatusColor {
            get { return color; }
            set { color = value; }
        }

        public string Name {
            get { return name; }
            set { name = value; }
        }

        public string Text {
            get { return text; }
            set { text = value; }
        }
    }
}
