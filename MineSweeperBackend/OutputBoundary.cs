﻿using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend {
    public interface OutputBoundary {
        void changeStatus(Point onPosition, State newStatus);
        void gameWon();
        void gameOver();
    }
}
