﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.Exceptions;
using MineSweeperBackend.Interactors;
using MineSweeperBackend.RequestModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.MineGeneratorTests {
    [TestFixture]
    class GenerateTests {
        private GameField gameField;
        private MineFactory generator;

        private static int width = 5;
        private static int height = 10;

        [SetUp]
        public void setup() {
            gameField = new GameField(width, height);
            generator = new MineFactory(gameField);
        }

        [Test]
        public void generate_zeroMines_ArrayLengthZeroExpected() {
            int numberOfMines = 0;
            HashSet<Point> mines = generator.generate(numberOfMines);
            Assert.AreEqual(0, mines.Count);
        }
        
        [Test]
        public void generate_fiveMines_PositionsNotNull() {
            HashSet<Point> mines = generator.generate(5);
            foreach(Point mine in mines) {
                Assert.NotNull(mine);
            }
        }

        [Test]
        public void generate_TooManyMines_TooManyMinesException() {
            Assert.Throws<OutOfGameFieldBoundaryException>(() => generator.generate(width * height + 1));
        }

        [Test]
        public void generate_NegativeAmountOfMines_NegativeAmountOfMinesException() {
            Assert.Throws<OutOfGameFieldBoundaryException>(() => generator.generate(-1));
        }
    }
}
