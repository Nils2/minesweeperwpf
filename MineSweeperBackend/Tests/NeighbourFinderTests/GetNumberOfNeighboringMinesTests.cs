﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.Interactors;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.NeighbourFinderTests {
    [TestFixture]
    class GetNumberOfNeighboringMinesTests {
        private GameField gamefield;
        private MineMockups mineMockUp;
        private NeighbourFinder finder;

        private static int width = 3;
        private static int height = 3;

        [SetUp]
        public void setup() {
            gamefield = new GameField(width, height);
            mineMockUp = new MineMockups();
            finder = new NeighbourFinder();
        }

        [Test]
        public void getNumberOfNeighboringMines_NoNeighboringMines_ZeroExpected() {
            HashSet<Point> mines = mineMockUp.getEmptyHashSet();
            Point point = new Point(1, 1);
            int numberOfMines = finder.getNumberOfNeighboringMines(mines, gamefield, point);
            Assert.AreEqual(0, numberOfMines);
        }

        [Test]
        public void getNumberOfNeighboringMines_EightNeighboringMines_EightExpected() {
            HashSet<Point> mines = mineMockUp.getMinesOnOuterEdgeOfThreeTimesThreeGameField();
            Point point = new Point(1, 1);
            int numberOfMines = finder.getNumberOfNeighboringMines(mines, gamefield, point);
            Assert.AreEqual(8, numberOfMines);
        }

        [Test]
        public void getNumberofNeighboringMines_ThreeNeighboringMines_ThreeExpected() {
            HashSet<Point> mines = mineMockUp.getMinesInUpperRow(width);
            Point point = new Point(1, 1);
            int numberOfMines = finder.getNumberOfNeighboringMines(mines, gamefield, point);
            Assert.AreEqual(3, numberOfMines);
        }
    }
}
