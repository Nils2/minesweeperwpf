﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.Interactors;
using MineSweeperBackend.RequestModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.NeighbourFinderTests {
    [TestFixture]
    class GetNeighboursTests {
        private GameField gamefield;
        private NeighbourFinder finder;

        private static int width = 5;
        private static int height = 5;

        [SetUp]
        public void setup() {
            gamefield = new GameField(width, height);
            finder = new NeighbourFinder();
        }

        [Test]
        public void getNeighbours_FieldInMiddle_EightNeighboursExpected() {
            Point center = new Point(2, 2);
            HashSet<Point> neighbours = finder.getNeighbours(gamefield, center);
            Assert.AreEqual(8, neighbours.Count);
        }

        [Test]
        public void getNeighbours_FieldUpperLeftCorner_3NeighboursExpected() {
            Point center = new Point(0, 0);
            HashSet<Point> neighbours = finder.getNeighbours(gamefield, center);
            Assert.AreEqual(3, neighbours.Count);
        }

        [Test]
        public void getNeighbours_FieldLowerRightCorner_3NeighboursExpected() {
            Point center = new Point(width - 1, height - 1);
            HashSet<Point> neighbours = finder.getNeighbours(gamefield, center);
            Assert.AreEqual(3, neighbours.Count);
        }
    }
}
