﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.Interactors;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.GameLogicTests {
    [TestFixture]
    class StartGameTests {
        private static int width = 4;
        private static int height = 5;
        private static int numberOfMines = 5;
        private GameLogic gameLogic;

        public object Gamefield { get; private set; }

        [SetUp]
        public void setup() {
            gameLogic = new GameLogic(new OutputBoundaryMockUp());
            gameLogic.startGame(width, height, numberOfMines);
        }

        [Test]
        public void gameFieldWidthAndHeight_WidthEquals4AndHeightEquals5Expected() {
            GameField gamefield = gameLogic.getGameField();
            Assert.AreEqual(width, gamefield.Width);
            Assert.AreEqual(height, gamefield.Height);
        }

        [Test]
        public void areMinesSet_HashsetWith5MinesExpected() {
            HashSet<Point> mines = gameLogic.getMInes();
            Assert.AreEqual(numberOfMines, mines.Count);
        }

        [Test]
        public void isStateIteratorSet_NotNullExpected() {
            StateIterator iterator = gameLogic.getStateIterator();
            Assert.NotNull(iterator);
        }
    }
}
