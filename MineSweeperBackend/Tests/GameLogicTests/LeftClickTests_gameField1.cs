﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.Tests.Misc;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.GameLogicTests {
    [TestFixture]
    class LeftClickTests_gameField1 {
        private GameLogic gamelogic;
        private GameField gamefield;
        private HashSet<Point> mines;
        private OutputBoundary ui;
        private GameFieldComparer comparer;

        [SetUp]
        public void setup() {
            comparer = new GameFieldComparer();
            ui = new OutputBoundaryMockUp();
            gamelogic = new GameLogic(ui);
            gamefield = GameLogicLeftClickMockUps.getGameField_2x2_withMineQuestionmarkFlaggedAndDefaultStates();
            mines = GameLogicLeftClickMockUps.getMines_2x2_X1AndY1NoMine();
            gamelogic.setValuesForTests(gamefield, mines);
        }

        [Test]
        public void leftClick_onFlaggedFieldWithMine_NoGameFieldChangeExpected() {
            gamelogic.leftClick(new Point(0, 1));
            GameField expected = GameLogicLeftClickMockUps.getGameField_2x2_withMineQuestionmarkFlaggedAndDefaultStates();
            Assert.True(comparer.areEqual(expected, gamelogic.getGameField()));
        }

        [Test]
        public void leftClick_onQuestionmarkFieldWithMine_NoGameFieldChangeExpected() {
            gamelogic.leftClick(new Point(1, 0));
            GameField expected = GameLogicLeftClickMockUps.getGameField_2x2_withMineQuestionmarkFlaggedAndDefaultStates();
            Assert.True(comparer.areEqual(expected, gamelogic.getGameField()));
        }

        [Test]
        public void leftClick_onDefaultFieldWithMine_GameOverExceptionExpected() {
            try {
                gamelogic.leftClick(new Point(0, 0));
                Assert.True(false);
            }
            catch (Exception ex) {
                Assert.AreEqual(OutputBoundaryMockUp.GAME_OVER_MESSAGE, ex.Message);
            }
        }

        [Test]
        public void leftClick_onDefaultFieldWithoutMine_GameWonMessageExpected() {
            try {
                gamelogic.leftClick(new Point(1, 1));
                Assert.True(false);
            } catch(Exception ex) {
                Assert.AreEqual(OutputBoundaryMockUp.GAME_WON_MESSAGE, ex.Message);
            }
        }
    }
}
