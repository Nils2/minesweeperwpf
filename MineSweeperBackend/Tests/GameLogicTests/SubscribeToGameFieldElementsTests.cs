﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.GameLogicTests {
    [TestFixture]
    class SubscribeToGameFieldElementsTests {
        private GameLogic gameLogic;
        private GameField gamefield;
        private IObserver<State>[,] uiElements;

        private static int width = 4;
        private static int height = 4;
        private static int numberOfMines = 0;

        [SetUp]
        public void setup() {
            gameLogic = new GameLogic(new OutputBoundaryMockUp());
            gameLogic.startGame(width, height, numberOfMines);
            gamefield = gameLogic.getGameField();
            uiElements = UiElementObserverMockups.getUiElementMockUp(width, height);
        }

        [Test]
        public void subscribeToGameFieldElements_ObserverInGameFieldFieldsNotNull() {
            gameLogic.subscribeToGameFieldElements(uiElements);
            for (int i = 0; i < width; i++) {
                for(int j = 0; j < height; j++) {
                    Point position = new Point(i, j);
                    Field field = gamefield[position];
                    Assert.NotNull(field.getObserver());
                }
            }
        }
    }
}
