﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.GameLogicTests {
    [TestFixture]
    class RightClickTests {
        private GameLogic gamelogic;

        private Point validPosition;

        [SetUp]
        public void setup() {
            gamelogic = new GameLogic(new OutputBoundaryMockUp());
            gamelogic.startGame(3, 3, 3);
            validPosition = new Point(0, 0);
        }

        [Test]
        public void rightClick_validPosition_FieldStateChangesToFlaggedState() {
            gamelogic.rightClick(validPosition);
            Field field = gamelogic.getGameField()[validPosition];
            Assert.AreEqual(field.getState().GetType(), typeof(FlaggedState));
        }
    }
}
