﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.Tests.Misc;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.GameLogicTests {
    [TestFixture]
    class LeftClickTests_gameField2 {
        private GameLogic gamelogic;
        private GameField gamefield;
        private HashSet<Point> mines;
        private OutputBoundary ui;
        private GameFieldComparer comparer;

        [SetUp]
        public void setup() {
            ui = new OutputBoundaryMockUp();
            gamelogic = new GameLogic(ui);
            gamefield = GameLogicLeftClickMockUps.getGameField_2x2_withBlankDefaultAndNeighboringMinesStates();
            mines = GameLogicLeftClickMockUps.getMines_2x2_MineAtX1AndY1();
            gamelogic.setValuesForTests(gamefield, mines);
            comparer = new GameFieldComparer();
        }

        [Test]
        public void leftClick_onUncoveredBlankField_noChangeInGameFieldExpected() {
            gamelogic.leftClick(new Point(0, 0));
            GameField expected = GameLogicLeftClickMockUps.getGameField_2x2_withBlankDefaultAndNeighboringMinesStates();
            Assert.True(comparer.areEqual(expected, gamelogic.getGameField()));
        }

        [Test]
        public void leftClick_onUncoveredNeighboringMineField_noChangeInGameFieldExpected() {
            gamelogic.leftClick(new Point(1, 0));
            GameField expected = GameLogicLeftClickMockUps.getGameField_2x2_withBlankDefaultAndNeighboringMinesStates();
            Assert.True(comparer.areEqual(expected, gamelogic.getGameField()));
        }
    }
}
