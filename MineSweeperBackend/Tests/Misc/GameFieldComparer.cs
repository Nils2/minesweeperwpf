﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.Misc {
    class GameFieldComparer {
        private GameField field1;
        private GameField field2;

        public bool areEqual(GameField field1, GameField field2) {
            this.field1 = field1;
            this.field2 = field2;
            if (!areEqualInWidthAndHeight()) {
                return false;
            }
            return areFieldsEqualInState();
        }

        private bool areEqualInWidthAndHeight() {
            return field1.Width == field2.Width && field1.Height == field2.Height;
        }

        private bool areFieldsEqualInState() {
            for(int i = 0; i < field1.Width; i++) {
                for(int j = 0; j < field1.Height; j++) {
                    Point position = new Point(i, j);
                    Type stateField1 = field1[position].getState().GetType();
                    Type stateField2 = field2[position].getState().GetType();
                    if(stateField1 != stateField2) {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
