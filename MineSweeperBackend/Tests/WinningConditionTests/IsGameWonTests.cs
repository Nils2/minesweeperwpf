﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.Interactors;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.WinningConditionTests {
    [TestFixture]
    class IsGameWonTests {
        private WinningCondition winningCondition;
        private GameField gamefield;

        [SetUp]
        public void setup() {
            winningCondition = new WinningCondition();
        }

        [TearDown]
        public void teardown() {
            winningCondition = null;
        }

        [Test]
        public void isGameWon_DefaultGameField_FalseExpected() {
            gamefield = WinningConditionGameFieldMockUps.getGameField_2X2_DefaultState();
            HashSet<Point> mines = WinningConditionGameFieldMockUps.getMines_MineAtX0AndY0();
            Assert.IsFalse(winningCondition.isGameWon(gamefield, mines));
        }

        [Test]
        public void isGameWon_EverythingUncoveredExceptMine_TrueExtected() {
            gamefield = WinningConditionGameFieldMockUps.getGameField_2x2_UpperLeftDefault();
            HashSet<Point> mines = WinningConditionGameFieldMockUps.getMines_MineAtX0AndY0();
            Assert.True(winningCondition.isGameWon(gamefield, mines));
        }

        [Test]
        public void isGameWon_OneDefaultFieldMissing_FalseExpected() {
            gamefield = WinningConditionGameFieldMockUps.getGameField_2x2_LowerRightDefault();
            HashSet<Point> mines = WinningConditionGameFieldMockUps.getMines_noMines();
            Assert.False(winningCondition.isGameWon(gamefield, mines));
        }

        [Test]
        public void isGameWon_FlaggedFieldWithMine_TrueExpected() {
            gamefield = WinningConditionGameFieldMockUps.getGameField_2x2_LowerRightFlaggedState();
            HashSet<Point> mines = WinningConditionGameFieldMockUps.getMines_MineAtX1AndY1();
            Assert.True(winningCondition.isGameWon(gamefield, mines));
        }

        [Test]
        public void isGameWon_QuestionMarkFieldWithMine_TrueExpected() {
            gamefield = WinningConditionGameFieldMockUps.getGameField_2x2_UpperLeftQuestionMarkState();
            HashSet<Point> mines = WinningConditionGameFieldMockUps.getMines_MineAtX0AndY0();
            Assert.True(winningCondition.isGameWon(gamefield, mines));
        }

        [Test]
        public void isGameWon_FlaggedFieldNoMine_FalseExpected() {
            gamefield = WinningConditionGameFieldMockUps.getGameField_2x2_LowerRightFlaggedState();
            HashSet<Point> mines = WinningConditionGameFieldMockUps.getMines_noMines();
            Assert.False(winningCondition.isGameWon(gamefield, mines));
        }

        [Test]
        public void isGameWon_QuestionMarkStateNoMine_FalseExpected() {
            gamefield = WinningConditionGameFieldMockUps.getGameField_2x2_UpperLeftQuestionMarkState();
            HashSet<Point> mines = WinningConditionGameFieldMockUps.getMines_noMines();
            Assert.False(winningCondition.isGameWon(gamefield, mines));
        }

        [Test]
        public void isGameWon_AllFieldsUncovered_TrueExpected() {
            gamefield = WinningConditionGameFieldMockUps.getGameField_2x2_AllFieldsUncovered();
            HashSet<Point> mines = WinningConditionGameFieldMockUps.getMines_noMines();
            Assert.True(winningCondition.isGameWon(gamefield, mines));
        }
    }
}
