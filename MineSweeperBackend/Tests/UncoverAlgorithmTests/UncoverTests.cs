﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.Interactors;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.UncoverAlgorithmTests {
    [TestFixture]
    class UncoverTests {
        private GameField gamefield;
        private UncoverAlgorithm algorithm;

        [SetUp]
        public void setup() {
            gamefield = GameFieldMockUpForUncoverAlgorithm.getGameField_5X5();
            HashSet<Point> mines = GameFieldMockUpForUncoverAlgorithm.getMines_5X5();
            algorithm = new UncoverAlgorithm(gamefield, mines);
        }

        [Test]
        public void uncover_clickAtX0AndY0() {
            algorithm.uncover(new Point(0, 0));
            Field[,] expectedField = GameFieldMockUpForUncoverAlgorithm.getGameFieldWhenAtX0AndY0Clicked();
            testIfTwoGameFieldsContainSameTypes(expectedField, gamefield.getGameField());
        }

        private void testIfTwoGameFieldsContainSameTypes(Field[,] expected, Field[,] actual) {
            for(int i = 0; i < expected.GetLength(0); i++) {
                for(int j = 0; j < expected.GetLength(1); j++) {
                    Assert.AreEqual(expected[i, j].getState().GetType(), actual[i,j].getState().GetType());
                }
            }
        }

        [Test]
        public void uncover_clickAtX4AndY4() {
            algorithm.uncover(new Point(4, 4));
            Field[,] expectedField = GameFieldMockUpForUncoverAlgorithm.getGameFieldWhenAtX4AndY4Clicked();
            testIfTwoGameFieldsContainSameTypes(expectedField, gamefield.getGameField());
        }

        [Test]
        public void uncover_clickAtX1AndY4() {
            algorithm.uncover(new Point(1, 4));
            Field[,] expectedField = GameFieldMockUpForUncoverAlgorithm.getGameFieldWhenAtX1AndY4Clicked();
            testIfTwoGameFieldsContainSameTypes(expectedField, gamefield.getGameField());
        }

        [Test]
        public void uncover_clickAtX1AndY1_FieldAtX1AndY1StateWithEightNeighboringMinesExpected() {
            algorithm = new UncoverAlgorithm(gamefield, GameFieldMockUpForUncoverAlgorithm.getMines_3X3_MinesAtOuterEdge());
            Point clicked = new Point(1, 1);
            algorithm.uncover(clicked);
            State expected = new UncoveredNeighboringMinesState(8);
            State actual = gamefield[clicked].getState();
            Assert.AreEqual(expected.Text, actual.Text);
        }
    }
}
