﻿using MineSweeperBackend.Interactors;
using MineSweeperBackend.ResponseModels;
using MineSweeperBackend.Tests.MockUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.StateIteratorTests {
    [TestFixture]
    class NextTests {
        private List<State> possibleStates;
        private StateIterator iterator;

        [SetUp]
        public void setup() {
            possibleStates = StateIteratorMockups.getFieldStatesForRightClick();
            iterator = new StateIterator(possibleStates);
        }

        [Test]
        public void next_iterateFromFirstElement_SecondElementExpected() {
            State state = iterator.next(possibleStates[0]);
            string expected = possibleStates[1].Name;
            Assert.AreEqual(expected, state.Name);
        }

        [Test]
        public void next_iterateFromLastElement_FirstElementExpected() {
            State state = iterator.next(possibleStates.Last());
            string expected = possibleStates[0].Name;
            Assert.AreEqual(expected, state.Name);
        }
    }
}
