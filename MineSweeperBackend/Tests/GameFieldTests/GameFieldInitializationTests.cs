﻿using MineSweeperBackend.BusinessObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.GameFieldTests {
    [TestFixture]
    class GameFieldInitializationTests {
        private GameField gameField;

        private static int width = 5;
        private static int height = 10;

        [SetUp]
        public void setup() {
            gameField = new GameField(width, height);
        }

        [Test]
        public void widthOfGameField() {
            Assert.AreEqual(width, gameField.getGameField().GetLength(0));
        }

        [Test]
        public void heightOfGameField() {
            Assert.AreEqual(height, gameField.getGameField().GetLength(1));
        }

        [Test]
        public void fieldsNotNull() {
            Field field = gameField.getGameField()[0, 0];
            Assert.NotNull(field);
        }
    }
}
