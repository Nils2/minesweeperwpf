﻿using MineSweeperBackend.BusinessObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.GameFieldTests {
    [TestFixture]
    class IsInGameFieldTests {
        private GameField gamefield;

        private static int width = 5;
        private static int height = 5;

        [SetUp]
        public void setup() {
            gamefield = new GameField(width, height);
        }

        [Test]
        public void isInGameField_negativeXPosition_FalseExpected() {
            Assert.False(gamefield.isInGameField(-1, 0));
        }

        [Test]
        public void isInGameField_xPositionTooGreat_FalseExpected() {
            Assert.False(gamefield.isInGameField(width, 0));
        }

        [Test]
        public void isInGameField_yPositionNegative_FalseExpected() {
            Assert.False(gamefield.isInGameField(0, -1));
        }

        [Test]
        public void isInGameField_yPositionTooGreat_FalseExpected() {
            Assert.False(gamefield.isInGameField(0, height));
        }

        [Test]
        public void isInGameField_MinPositions_TrueExpected() {
            Assert.True(gamefield.isInGameField(0, 0));
        }

        [Test]
        public void isInGameField_MaxPositions_TrueExpected() {
            Assert.True(gamefield.isInGameField(width - 1, height - 1));
        }
    }
}
