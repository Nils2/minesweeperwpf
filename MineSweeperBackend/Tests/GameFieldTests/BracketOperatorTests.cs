﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.GameFieldTests {
    [TestFixture]
    class BracketOperatorTests {
        private GameField gameField;

        private static int width = 5;
        private static int height = 5;

        [SetUp]
        public void setup() {
            gameField = new GameField(width, height);
        }

        [Test]
        public void bracketOperator_indexInGameFieldRange_FieldAtXZeroAndYZeroExpected() {
            Point position = new Point(0, 0);
            Field result = gameField[position];
            Assert.True(position == result.getPosition());
        }

        [Test]
        public void bracketOperator_getFieldInBottomRightCorner_FieldExpeceted() {
            Point position = new Point(width - 1, height - 1);
            Field result = gameField[position];
            Assert.True(position == result.getPosition());
        }
    }
}
