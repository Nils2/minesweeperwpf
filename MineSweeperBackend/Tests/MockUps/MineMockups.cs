﻿using MineSweeperBackend.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.MockUps {
    class MineMockups {
        HashSet<Point> mines;

        /// <summary>
        /// Generates a Hashset that contains points in all columns of the top row of the gamefield
        /// </summary>
        /// <param name="width">number of columns</param>
        /// <returns>A hashset with the points of the mines</returns>
        public HashSet<Point> getMinesInUpperRow(int width) {
            mines = new HashSet<Point>();
            for (int i = 0; i < width; i++) {
                mines.Add(new Point(i, 0));
            }
            return mines;
        }

        public HashSet<Point> getMinesOnOuterEdgeOfThreeTimesThreeGameField() {
            mines = new HashSet<Point>();
            mines.Add(new Point(0, 0));
            mines.Add(new Point(1, 0));
            mines.Add(new Point(2, 0));
            mines.Add(new Point(0, 1));
            mines.Add(new Point(2, 1));
            mines.Add(new Point(0, 2));
            mines.Add(new Point(1, 2));
            mines.Add(new Point(2, 2));
            return mines;
        }

        public HashSet<Point> getEmptyHashSet() {
            return new HashSet<Point>();
        }
    }
}
