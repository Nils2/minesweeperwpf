﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.MockUps {
    class GameLogicLeftClickMockUps {

        /* GameField: F = FlaggedState, ? = QuestionmarkState, D = DefaultState
         * D ?
         * F D
         */
        public static GameField getGameField_2x2_withMineQuestionmarkFlaggedAndDefaultStates() {
            GameField gamefield = new GameField(2, 2);
            gamefield[new Point(0, 0)].updateState(new DefaultState());
            gamefield[new Point(1, 0)].updateState(new QuestionmarkState());
            gamefield[new Point(0, 1)].updateState(new FlaggedState());
            gamefield[new Point(1, 1)].updateState(new DefaultState());
            return gamefield;
        }

        /* Mines on Gamefield: X = Mine, O = no mine
         * X X
         * X O
         */
        public static HashSet<Point> getMines_2x2_X1AndY1NoMine() {
            HashSet<Point> mines = new HashSet<Point>();
            mines.Add(new Point(0, 0));
            mines.Add(new Point(1, 0));
            mines.Add(new Point(0, 1));
            return mines;
        }

        /*  GameField: B= UncoveredBlankState, 1 = UncoveredNeighboringMineState, D = DefaultState
         * B 1
         * 1 D
         */
        public static GameField getGameField_2x2_withBlankDefaultAndNeighboringMinesStates() {
            GameField field = new GameField(2, 2);
            field[new Point(0, 0)].updateState(new UncoveredBlankState());
            field[new Point(1, 0)].updateState(new UncoveredNeighboringMinesState(1));
            field[new Point(0, 1)].updateState(new UncoveredNeighboringMinesState(1));
            field[new Point(1, 1)].updateState(new DefaultState());
            return field;
        }

        public static HashSet<Point> getMines_noMines() {
            return new HashSet<Point>();
        }

        /* Mines on Gamefield: X = Mine, O = no mine
        * O O
        * O X
        */
        public static HashSet<Point> getMines_2x2_MineAtX1AndY1() {
            HashSet<Point> mines = new HashSet<Point>();
            mines.Add(new Point(1, 1));
            return mines;
        }
    }
}
