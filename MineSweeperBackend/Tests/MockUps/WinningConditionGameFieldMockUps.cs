﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.MockUps {
    class WinningConditionGameFieldMockUps {

        public static GameField getGameField_2X2_DefaultState() {
            return new GameField(2, 2);
        }

        public static GameField getGameField_2x2_AllFieldsUncovered() {
            GameField gamefield = new GameField(2, 2);
            gamefield[new Point(0, 0)].updateState(new UncoveredBlankState());
            gamefield[new Point(0, 1)].updateState(new UncoveredBlankState());
            gamefield[new Point(1, 0)].updateState(new UncoveredBlankState());
            gamefield[new Point(1, 1)].updateState(new UncoveredBlankState());
            return gamefield;
        }

        /* GameField: D = DefaultState, U = uncovered
        * D U
        * U U
        */
        public static GameField getGameField_2x2_UpperLeftDefault() {
            GameField gamefield = new GameField(2, 2);
            gamefield[new Point(1, 0)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(0, 1)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(1, 1)].updateState(new UncoveredNeighboringMinesState(1));
            return gamefield;
        }

        /* GameField: ? = QuestionmarkState, U = uncovered
        * ? U
        * U U
        */
        public static GameField getGameField_2x2_UpperLeftQuestionMarkState() {
            GameField gamefield = new GameField(2, 2);
            gamefield[new Point(1, 0)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(0, 1)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(1, 1)].updateState(new UncoveredNeighboringMinesState(1));
            return gamefield;
        }

        /*
         * Gamefield: D = DefaultState, U = uncovered
         * U U
         * U D 
         */
        public static GameField getGameField_2x2_LowerRightDefault() {
            GameField gamefield = new GameField(2, 2);
            gamefield[new Point(0, 0)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(0, 1)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(1, 0)].updateState(new UncoveredNeighboringMinesState(1));
            return gamefield;
        }

        /*
         * Gamefield: F = FlaggedState, U = uncovered
         * U U
         * U F 
         */
        public static GameField getGameField_2x2_LowerRightFlaggedState() {
            GameField gamefield = new GameField(2, 2);
            gamefield[new Point(0, 0)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(0, 1)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(1, 0)].updateState(new UncoveredNeighboringMinesState(1));
            gamefield[new Point(1, 1)].updateState(new FlaggedState());
            return gamefield;
        }

        /* Mines in GameField: X = Mine, O = no Mine
         * O O
         * O O
         * */
        public static HashSet<Point> getMines_noMines() {
            return new HashSet<Point>();
        }
        
        /* Mines in GameField: X = Mine, O = no Mine
         * X O
         * O O
         * */
        public static HashSet<Point> getMines_MineAtX0AndY0() {
            return new HashSet<Point>() { new Point(0, 0) };
        }

        /* Mines in GameField: X = Mine, O = no Mine
         * O O 
         * O X
         */
         public static HashSet<Point> getMines_MineAtX1AndY1() {
            return new HashSet<Point>() { new Point(1, 1) };
        }
    }
}
