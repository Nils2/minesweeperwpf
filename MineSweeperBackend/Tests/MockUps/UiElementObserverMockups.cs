﻿using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.MockUps {
    public class UiElementObserverMockups {
        public class uiELement : IObserver<State> {
            public void OnCompleted() {
                throw new NotImplementedException();
            }

            public void OnError(Exception error) {
                throw new NotImplementedException();
            }

            public void OnNext(State value) {
                return;
            }
        }

        public static IObserver<State>[,] getUiElementMockUp(int width, int height) {
            uiELement[,] elements = new uiELement[width, height];
            for(int i = 0; i < width; i++) {
                for(int j = 0; j < height; j++) {
                    elements[i, j] = new uiELement();
                }
            }
            return elements;
        }
    }
}
