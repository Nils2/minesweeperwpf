﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;

namespace MineSweeperBackend.Tests.MockUps {
    class OutputBoundaryMockUp : OutputBoundary {
        public static string GAME_OVER_MESSAGE = "Game Over";
        public static string GAME_WON_MESSAGE = "Game Won";

        public void changeStatus(Point onPosition, State newStatus) {
            throw new NotImplementedException();
        }

        public void gameOver() {
            throw new Exception(GAME_OVER_MESSAGE);
        }

        public void gameWon() {
            throw new Exception(GAME_WON_MESSAGE);
        }
    }
}
