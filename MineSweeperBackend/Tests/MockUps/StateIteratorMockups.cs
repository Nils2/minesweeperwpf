﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.MockUps {
    class StateIteratorMockups {
        public static List<State> getFieldStatesForRightClick() {
            List<State> states = new List<State>();
            states.Add(new DefaultState());
            states.Add(new FlaggedState());
            states.Add(new QuestionmarkState());
            return states;
        }
    }
}
