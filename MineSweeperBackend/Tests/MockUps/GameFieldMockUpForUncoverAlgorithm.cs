﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.MockUps {
    class GameFieldMockUpForUncoverAlgorithm {

        public static GameField getGameField_5X5() {
            return new GameField(5, 5);
        }

        public static GameField getGameField_3X3() {
            return new GameField(3, 3);
        }

        /*
        * GameField for most tests: X = Mine, O = No Mine
        * O O X O O
        * O O O O O
        * X O O O O
        * O O O X O
        * O O O O O
        */
        public static HashSet<Point> getMines_5X5() {
            HashSet<Point> mines = new HashSet<Point>();
            mines.Add(new Point(2, 0));
            mines.Add(new Point(0, 2));
            mines.Add(new Point(3, 3));
            return mines;
        }


        /*
         *GameField : X = Mine, O = No Mine
         * X X X
         * X O X
         * X X X
         */
        public static HashSet<Point> getMines_3X3_MinesAtOuterEdge() {
            HashSet<Point> mines = new HashSet<Point>();
            mines.Add(new Point(0, 0));
            mines.Add(new Point(1, 0));
            mines.Add(new Point(2, 0));
            mines.Add(new Point(0, 1));
            mines.Add(new Point(2, 1));
            mines.Add(new Point(0, 2));
            mines.Add(new Point(1, 2));
            mines.Add(new Point(2, 2));
            return mines;
        }

        public static Field[,] getGameFieldWhenAtX0AndY0Clicked() {
            Field[,] uncoveredField = new Field[5, 5];
            uncoveredField[0, 0] = new Field(new Point(0, 0), new UncoveredBlankState());
            uncoveredField[1, 0] = new Field(new Point(0, 1), new UncoveredNeighboringMinesState(1));
            uncoveredField[2, 0] = new Field(new Point(0, 2), new DefaultState());
            uncoveredField[3, 0] = new Field(new Point(3, 0), new DefaultState());
            uncoveredField[4, 0] = new Field(new Point(4, 0), new DefaultState());

            uncoveredField[0, 1] = new Field(new Point(0, 1), new UncoveredNeighboringMinesState(1));
            uncoveredField[1, 1] = new Field(new Point(1, 1), new UncoveredNeighboringMinesState(2));
            uncoveredField[2, 1] = new Field(new Point(2, 1), new DefaultState());
            uncoveredField[3, 1] = new Field(new Point(3, 1), new DefaultState());
            uncoveredField[4, 1] = new Field(new Point(4, 1), new DefaultState());

            uncoveredField[0, 2] = new Field(new Point(0, 2), new DefaultState());
            uncoveredField[1, 2] = new Field(new Point(1, 2), new DefaultState());
            uncoveredField[2, 2] = new Field(new Point(2, 2), new DefaultState());
            uncoveredField[3, 2] = new Field(new Point(3, 2), new DefaultState());
            uncoveredField[4, 2] = new Field(new Point(4, 2), new DefaultState());

            uncoveredField[0, 3] = new Field(new Point(0, 3), new DefaultState());
            uncoveredField[1, 3] = new Field(new Point(1, 3), new DefaultState());
            uncoveredField[2, 3] = new Field(new Point(2, 3), new DefaultState());
            uncoveredField[3, 3] = new Field(new Point(3, 3), new DefaultState());
            uncoveredField[4, 3] = new Field(new Point(4, 3), new DefaultState());

            uncoveredField[0, 4] = new Field(new Point(0, 4), new DefaultState());
            uncoveredField[1, 4] = new Field(new Point(1, 4), new DefaultState());
            uncoveredField[2, 4] = new Field(new Point(2, 4), new DefaultState());
            uncoveredField[3, 4] = new Field(new Point(3, 4), new DefaultState());
            uncoveredField[4, 4] = new Field(new Point(4, 4), new DefaultState());

            return uncoveredField;
        }

        public static Field[,] getGameFieldWhenAtX4AndY4Clicked() {
            Field[,] uncoveredField = new Field[5, 5];
            uncoveredField[0, 0] = new Field(new Point(0, 0), new DefaultState());
            uncoveredField[1, 0] = new Field(new Point(0, 1), new DefaultState());
            uncoveredField[2, 0] = new Field(new Point(0, 2), new DefaultState());
            uncoveredField[3, 0] = new Field(new Point(3, 0), new DefaultState());
            uncoveredField[4, 0] = new Field(new Point(4, 0), new DefaultState());

            uncoveredField[0, 1] = new Field(new Point(0, 1), new DefaultState());
            uncoveredField[1, 1] = new Field(new Point(1, 1), new DefaultState());
            uncoveredField[2, 1] = new Field(new Point(2, 1), new DefaultState());
            uncoveredField[3, 1] = new Field(new Point(3, 1), new DefaultState());
            uncoveredField[4, 1] = new Field(new Point(4, 1), new DefaultState());

            uncoveredField[0, 2] = new Field(new Point(0, 2), new DefaultState());
            uncoveredField[1, 2] = new Field(new Point(1, 2), new DefaultState());
            uncoveredField[2, 2] = new Field(new Point(2, 2), new DefaultState());
            uncoveredField[3, 2] = new Field(new Point(3, 2), new DefaultState());
            uncoveredField[4, 2] = new Field(new Point(4, 2), new DefaultState());

            uncoveredField[0, 3] = new Field(new Point(0, 3), new DefaultState());
            uncoveredField[1, 3] = new Field(new Point(1, 3), new DefaultState());
            uncoveredField[2, 3] = new Field(new Point(2, 3), new DefaultState());
            uncoveredField[3, 3] = new Field(new Point(3, 3), new DefaultState());
            uncoveredField[4, 3] = new Field(new Point(4, 3), new DefaultState());

            uncoveredField[0, 4] = new Field(new Point(0, 4), new DefaultState());
            uncoveredField[1, 4] = new Field(new Point(1, 4), new DefaultState());
            uncoveredField[2, 4] = new Field(new Point(2, 4), new DefaultState());
            uncoveredField[3, 4] = new Field(new Point(3, 4), new DefaultState());
            uncoveredField[4, 4] = new Field(new Point(4, 4), new UncoveredNeighboringMinesState(1));

            return uncoveredField;
        }

        public static Field[,] getGameFieldWhenAtX1AndY4Clicked() {
            Field[,] uncoveredField = new Field[5, 5];
            uncoveredField[0, 0] = new Field(new Point(0, 0), new DefaultState());
            uncoveredField[1, 0] = new Field(new Point(0, 1), new DefaultState());
            uncoveredField[2, 0] = new Field(new Point(0, 2), new DefaultState());
            uncoveredField[3, 0] = new Field(new Point(3, 0), new DefaultState());
            uncoveredField[4, 0] = new Field(new Point(4, 0), new DefaultState());

            uncoveredField[0, 1] = new Field(new Point(0, 1), new DefaultState());
            uncoveredField[1, 1] = new Field(new Point(1, 1), new DefaultState());
            uncoveredField[2, 1] = new Field(new Point(2, 1), new DefaultState());
            uncoveredField[3, 1] = new Field(new Point(3, 1), new DefaultState());
            uncoveredField[4, 1] = new Field(new Point(4, 1), new DefaultState());

            uncoveredField[0, 2] = new Field(new Point(0, 2), new DefaultState());
            uncoveredField[1, 2] = new Field(new Point(1, 2), new DefaultState());
            uncoveredField[2, 2] = new Field(new Point(2, 2), new DefaultState());
            uncoveredField[3, 2] = new Field(new Point(3, 2), new DefaultState());
            uncoveredField[4, 2] = new Field(new Point(4, 2), new DefaultState());

            uncoveredField[0, 3] = new Field(new Point(0, 3), new UncoveredNeighboringMinesState(1));
            uncoveredField[1, 3] = new Field(new Point(1, 3), new UncoveredNeighboringMinesState(1));
            uncoveredField[2, 3] = new Field(new Point(2, 3), new UncoveredNeighboringMinesState(1));
            uncoveredField[3, 3] = new Field(new Point(3, 3), new DefaultState());
            uncoveredField[4, 3] = new Field(new Point(4, 3), new DefaultState());

            uncoveredField[0, 4] = new Field(new Point(0, 4), new UncoveredBlankState());
            uncoveredField[1, 4] = new Field(new Point(1, 4), new UncoveredBlankState());
            uncoveredField[2, 4] = new Field(new Point(2, 4), new UncoveredNeighboringMinesState(1));
            uncoveredField[3, 4] = new Field(new Point(3, 4), new DefaultState());
            uncoveredField[4, 4] = new Field(new Point(4, 4), new DefaultState());

            return uncoveredField;
        }
    }
}
