﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.BusinessObjects.Factories;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.FieldFactoryTests {
    [TestFixture]
    class CreateInstanceTests {
        private FieldFactory factory;
        
        [SetUp]
        public void setup() {
            factory = new FieldFactory();
        }

        [Test]
        public void createInstance_validPosition_identicalPositionExpected() {
            Point point = new Point(0, 0);
            Field result = factory.createInstance(point);
            Assert.AreEqual(point, result.getPosition());
        }

        [Test]
        public void createInstance_validPosition_defaultStatusExpected() {
            Point point = new Point(0, 0);
            Field result = factory.createInstance(point);
            Type expected = new DefaultState().GetType();
            Assert.AreEqual(expected, result.getState().GetType());
        }
    }
}
