﻿using MineSweeperBackend.BusinessObjects;
using MineSweeperBackend.RequestModels;
using MineSweeperBackend.ResponseModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeperBackend.Tests.FieldTests {
    [TestFixture]
    class UpdateStateTests {
        private Field field;
        private ObserverObject observer;
        private IDisposable disposable;
        private class ObserverObject : IObserver<State> {
            public State currentState;

            public void OnCompleted() {
                throw new NotImplementedException();
            }

            public void OnError(Exception error) {
                throw new NotImplementedException();
            }

            public void OnNext(State value) {
                currentState = value;
            }
        }

        [SetUp]
        public void setup() {
            field = new Field(new Point(0, 0), new DefaultState());
            observer = new ObserverObject();
            disposable = field.Subscribe(observer);
        }

        [Test]
        public void updateState_toFlaggedState_FlaggedStateAsFieldPropertyExpected() {
            State flagged = new FlaggedState();
            field.updateState(flagged);
            Assert.AreEqual(flagged.Name, field.getState().Name);
        }

        [Test]
        public void updateState_toQuestionmarkState_QuestionmarkStateInObserverObjectExpected() {
            State question = new QuestionmarkState();
            field.updateState(question);
            Assert.AreEqual(question.Name, observer.currentState.Name);
        }

        [Test]
        public void updateState_noObserverSubscribtion_NoExceptionExpected() {
            Field field = new Field(new Point(0, 0), new DefaultState());
            field.updateState(new UncoveredBlankState());
        }
    }
}
